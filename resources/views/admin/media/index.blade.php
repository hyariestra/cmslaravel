@extends('layouts.admin')



@section('content')

<h1>Media</h1>

@if($photos)

<form class="form-inline" action="delete/media" method="post">

	{{csrf_field()}}
	{{method_field('delete')}}

	<div class="form-group">
		<select class="form-control" name="checkBoxArray" >
			<option value="delete">Delete</option>
		</select>
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary">
	</div>
	<hr>

	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th><input type="checkbox" id="options" name=""> </th>
				<th>Id</th>
				<th>Name</th>
				<th>Created</th>
				<th>Action</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($photos as $photo)

			<tr>
				<td><input class="checkBoxes" type="checkbox"  name="checkBoxArray[]" value="{{$photo->id}}" ></td>
				<td>{{$photo->id}}</td>
				<td><img height="50" src="{{$photo->file}}"> </td>
				<td>{{$photo->created_at?$photo->created_at:'no date'}}</td>
				<td>
					{!! Form::open(['method'=>'DELETE','action'=>['AdminMediasController@destroy',$photo->id]]) !!}

					<div class="form-group">
						{!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
					</div>

					{!! Form::close() !!}
				</td>

			</tr>
			@endforeach
		</tbody>
	</table>

</form>

@endif
@stop
@section('scripts')

<script>
	
$( document ).ready(function() {
    console.log( "ready!" );
});


$('#options').click(function(){

	if (this.checked) {
		$('.checkBoxes').each(function(){
			this.checked = true;
		});
	}else{
		$('.checkBoxes').each(function(){
			this.checked = false;
		});
	}

});


</script>

@stop
