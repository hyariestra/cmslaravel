﻿1
00:00:00,060 --> 00:00:05,080
Welcome back these students in this new section we are going to be creating a new feature for application

2
00:00:05,080 --> 00:00:08,630
is going to be a multi delete for a media page.

3
00:00:08,820 --> 00:00:15,560
So if you go to our media page what I want to do is I want to put some radio boxes Schir or check boxes.

4
00:00:15,600 --> 00:00:17,220
And I want to get one for one here.

5
00:00:17,220 --> 00:00:22,680
And when I select this one I want all of these to select the centime or if I check them individually

6
00:00:22,740 --> 00:00:28,080
or I check like say one two and three and I click delete then it would delete three of them.

7
00:00:28,140 --> 00:00:28,570
OK.

8
00:00:28,620 --> 00:00:34,470
So I think that's a pretty cool feature to add to her application so let's get let's get it and let's

9
00:00:34,980 --> 00:00:36,950
let's start doing it.

10
00:00:36,990 --> 00:00:40,290
So first of all we get to see where we are going to be putting this stuff here.

11
00:00:40,500 --> 00:00:45,880
First of all I think the admin meatiest controller is the right place to do this.

12
00:00:45,940 --> 00:00:46,250
OK.

13
00:00:46,260 --> 00:00:52,960
So we need to have meant here that we take that we also need to construct a form in a table.

14
00:00:52,980 --> 00:00:54,540
Actually we have a table.

15
00:00:55,170 --> 00:00:59,580
We need a form to submit that data so we need to go to that media view.

16
00:00:59,880 --> 00:01:07,910
So let's go to resources and media and it would be this guy here.

17
00:01:08,280 --> 00:01:09,810
OK.

18
00:01:09,810 --> 00:01:16,740
So yeah we have the table and we could put a form here.

19
00:01:17,550 --> 00:01:24,140
And I just want a regular form on just going to take this and

20
00:01:28,040 --> 00:01:30,680
what are the wood in the bottom of that.

21
00:01:30,900 --> 00:01:39,800
There this is going to be met post and the lead

22
00:01:43,850 --> 00:01:50,850
media I think we might have a action with that.

23
00:01:51,100 --> 00:01:53,340
Let's see a route.

24
00:01:53,410 --> 00:01:57,910
So let's go to Reynolds real quick and just make sure that we don't have anything in there that has

25
00:01:57,910 --> 00:02:02,010
to the media delete me.

26
00:02:02,110 --> 00:02:03,870
Media OK that's fine.

27
00:02:08,920 --> 00:02:10,090
School here.

28
00:02:10,200 --> 00:02:10,480
It's

29
00:02:13,670 --> 00:02:14,930
OK.

30
00:02:15,180 --> 00:02:17,430
We're just going to quote it like this just for now.

31
00:02:17,430 --> 00:02:18,100
Really.

32
00:02:19,170 --> 00:02:19,740
Doesn't matter.

33
00:02:19,740 --> 00:02:21,330
You can change it to whatever you want.

34
00:02:21,330 --> 00:02:21,890
OK.

35
00:02:22,080 --> 00:02:24,300
I'm just going to use that and

36
00:02:27,370 --> 00:02:32,470
let's see if we can create this new route here.

37
00:02:36,880 --> 00:02:38,470
It's going to be a post requests

38
00:02:41,230 --> 00:02:41,700
delete

39
00:02:44,370 --> 00:02:45,600
see this go back to it.

40
00:02:45,670 --> 00:02:46,850
Whoops sorry about that.

41
00:02:48,480 --> 00:02:49,920
Delete media OK.

42
00:02:50,160 --> 00:02:51,580
Just making sure I got the right

43
00:02:56,550 --> 00:03:05,880
and of course is going to control her and that's going to admin medius controller I believe will actually

44
00:03:05,880 --> 00:03:07,080
have it right here.

45
00:03:08,430 --> 00:03:09,420
No that's got to go.

46
00:03:09,420 --> 00:03:10,400
Sorry about that.

47
00:03:10,470 --> 00:03:17,000
This is guy and this actually put this in the bottom here.

48
00:03:22,350 --> 00:03:27,700
And we want this to go to some type of method.

49
00:03:29,950 --> 00:03:37,920
Elite media are just going to call it I'm going to give the same name so

50
00:03:46,000 --> 00:03:49,120
OK elite media.

51
00:03:49,630 --> 00:03:52,450
And we want to receive the requests here.

52
00:03:54,440 --> 00:03:57,950
And for now we're just going to dump this.

53
00:03:58,220 --> 00:04:00,630
Make sure that we are getting something.

54
00:04:01,400 --> 00:04:08,130
Let's go back.

55
00:04:08,230 --> 00:04:13,860
Let's go to the Hedding.

56
00:04:14,090 --> 00:04:22,160
We need a input here and it's going to be a checkbox

57
00:04:24,510 --> 00:04:25,230
and

58
00:04:28,640 --> 00:04:37,920
this input is going to have some type of ID and it's called options and that's it.

59
00:04:38,140 --> 00:04:41,190
That's all we want is input to to have actually.

60
00:04:41,680 --> 00:04:46,210
And right below another T.

61
00:04:46,340 --> 00:04:49,350
Let's put that to the right here.

62
00:04:49,550 --> 00:04:55,030
Another input here and what we want this to be is a checkbox as well.

63
00:04:57,250 --> 00:05:09,080
And then let's give it a name let's just say checkbox and say something like this we need to make these

64
00:05:09,080 --> 00:05:10,080
an array of course

65
00:05:12,590 --> 00:05:16,670
and we're going to have to give it a value and can you guess what the value is going to be.

66
00:05:19,790 --> 00:05:23,960
While the value is going to be what do we have here forto.

67
00:05:24,030 --> 00:05:26,600
So it's going to be the photo ID

68
00:05:29,450 --> 00:05:31,750
photo ID.

69
00:05:31,760 --> 00:05:34,560
Simple as that OK.

70
00:05:35,980 --> 00:05:37,930
So let me see if I have that correctly.

71
00:05:37,930 --> 00:05:41,070
Value the name.

72
00:05:41,180 --> 00:05:51,060
Let's also give it a class so we can identify this with Jake where we laid on.

73
00:05:51,120 --> 00:05:55,110
So let's call this check boxes.

74
00:05:57,760 --> 00:05:58,870
OK.

75
00:05:59,040 --> 00:06:00,030
Then this will work.

76
00:06:00,030 --> 00:06:03,660
Right now the way we have it let's go to form post

77
00:06:06,420 --> 00:06:09,100
let's try this.

78
00:06:09,210 --> 00:06:11,800
Actually we need some type of button here right.

79
00:06:11,970 --> 00:06:14,150
So we don't even have the inputs yet.

80
00:06:14,150 --> 00:06:26,750
So let's let's do a select here like this and let's call this checkbox array.

81
00:06:27,070 --> 00:06:28,840
Doesn't send name that we gave

82
00:06:36,140 --> 00:06:37,360
this guy right here.

83
00:06:39,170 --> 00:06:40,320
Checkbox array.

84
00:06:43,330 --> 00:06:45,720
OK that should be good.

85
00:06:45,780 --> 00:06:47,490
We're collecting there.

86
00:06:47,850 --> 00:06:50,790
And he said he select We have some options.

87
00:06:53,090 --> 00:06:59,130
And you know the value of this would be the least and delete.

88
00:06:59,290 --> 00:07:00,450
Simple.

89
00:07:01,140 --> 00:07:02,270
OK.

90
00:07:02,920 --> 00:07:06,750
Here this button we don't have to do much with is Bun.

91
00:07:07,090 --> 00:07:18,630
I just want to change the class VTM primary and maybe we could change this form to have a different

92
00:07:19,220 --> 00:07:22,100
in line let me see if I remember

93
00:07:24,880 --> 00:07:26,800
actually starts with form

94
00:07:30,450 --> 00:07:32,090
home my editor is not helping me

95
00:07:34,730 --> 00:07:36,010
OK.

96
00:07:37,290 --> 00:07:39,510
Let's go back here.

97
00:07:39,960 --> 00:07:41,390
And why not.

98
00:07:41,700 --> 00:07:46,200
OK we've got to work on dishtowels styles right here a little bit.

99
00:07:46,200 --> 00:07:49,290
So let's go back here and give this select

100
00:07:52,760 --> 00:07:56,110
class form control

101
00:07:59,230 --> 00:08:00,060
that we go.

102
00:08:00,370 --> 00:08:01,340
OK.

103
00:08:01,770 --> 00:08:03,770
Now we have these checkboxes.

104
00:08:04,140 --> 00:08:12,610
We need one check but actually a ball of here I thought I did that already.

105
00:08:12,610 --> 00:08:23,830
I guess I did not I was supposed to be this guy here and he's now showing on top Oh that's because I

106
00:08:23,830 --> 00:08:26,410
have that backwards.

107
00:08:26,430 --> 00:08:29,620
Jesus ok.

108
00:08:30,470 --> 00:08:31,150
There we go.

109
00:08:31,160 --> 00:08:32,910
Now we have this check box right here.

110
00:08:33,020 --> 00:08:33,620
OK.

111
00:08:33,620 --> 00:08:40,190
So we were finished setting this up for now configuring this well in the next lecture we try to take

112
00:08:40,190 --> 00:08:41,750
care of that Jake wari.

113
00:08:42,110 --> 00:08:42,830
OK.

114
00:08:43,340 --> 00:08:48,600
So that way when we click on one of these boxes it selects all of them where we click on this box.

115
00:08:48,710 --> 00:08:49,960
He selects all of them.

116
00:08:50,230 --> 00:08:50,710
OK.

117
00:08:50,840 --> 00:08:55,400
And we also want to make sure that this form is working sending data to our control it right to our

118
00:08:55,400 --> 00:08:57,220
method in our controller.

119
00:08:57,590 --> 00:08:59,850
So I'll see you in the next lecture.

